class User < ApplicationRecord
  has_secure_password
  validates :name, presence: true, length: {maximum: 50}
  validates :password, presence: true, length: {minimum: 6},on: :create
  has_many :addresses
  validates :email, presence: true, length: {maximum: 260}, uniqueness: {case_sensitive: false}
  before_save { self.email = email.downcase }
  
end
