class Food < ApplicationRecord
    validates :name,presence: true
    validates :price,presence: true
    validates :photo,presence: true
    validates :info,presence: true
end
