class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :redirect, only: [:show,:index]
  before_action :block_not_logged, except: [:create, :new]
  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id]) 
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        format.html { redirect_to sign_in_path, notice: 'Usuario foi criado com sucesso.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user = User.find(params[:id]) 
    if @user.update_attributes(user_params)
      redirect_to users_path
    else
      render action: :edit
    end
   end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'Usuario destruido com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :name, :admistrador, :password, :password_confirmation)
    end
    def user_params_without_password
      user_params.delete(:password)
      user_params.delete(:password_confirmation)
      user_params
   end
   def redirect
    if logged_in?
      if (@user!=@current_user && @current_user.admistrador!=true)
        redirect_to @current_user
      end
    end
   end
   def block_not_logged
    if !logged_in?
      redirect_to root_path
    end
  end

  end