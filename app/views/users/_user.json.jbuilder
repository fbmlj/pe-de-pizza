json.extract! user, :id, :email, :name, :admistrador, :created_at, :updated_at
json.url user_url(user, format: :json)
