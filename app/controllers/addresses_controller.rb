class AddressesController < ApplicationController
  before_action :set_address, only: [:show, :edit, :update, :destroy]
  def create
    @address = Address.new(address_params)
    @address.user_id=@current_user.id
    respond_to do |format|
      if @address.save
        format.html { redirect_to @current_user, notice: 'Address was successfully created.' }

        format.json { render :show, status: :created, location: @address }
      else
        format.html { render :new }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  def new
    @address = Address.new
  end

  def edit
  end

  def update
    @address = Address.find(params[:id]) 
    if @address.update_attributes(address_params)
      redirect_to user_path(@current_user)
    else
      render action: :edit
    end
  end

  def destroy
    @address.destroy
    respond_to do |format|
      format.html { redirect_to @current_user, notice: 'Address was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  private
  def set_address
    @address = Address.find(params[:id])
  end
    def address_params
      params.require(:address).permit(:city, :street, :number, :complement, :cep, :user_id)
    end

  end
