class Address < ApplicationRecord
  belongs_to :user
  validates :city, presence: true
  validates :street, presence: true
  validates :number, presence: true
  validates :cep, presence: true
end
