class CreateFoods < ActiveRecord::Migration[5.2]
  def change
    create_table :foods do |t|
      t.string :name
      t.float :price
      t.text :info
      t.string :photo
      t.string :kind

      t.timestamps
    end
  end
end
