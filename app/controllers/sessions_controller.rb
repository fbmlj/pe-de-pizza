class SessionsController < ApplicationController
  before_action :block_acess, except: [:destroy]
  before_action :redirect, only: [:sign_in]
  def new
  end

  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      sign_in(@user)
      redirect_to @user
    else
      render 'new'
    end 
  end
  
  def destroy
    sign_out
    redirect_to root_url
  end
  def redirect
    if !logged_in?
      redirect @current_user
    end

  end
end